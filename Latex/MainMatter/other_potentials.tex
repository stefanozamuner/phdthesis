\chapter{Devising new potentials}\label{ch:others}
\Glspl{kbp} have proved to be a simple and fast tool for recognizing the native state of proteins (see \cref{ch:kbp}) and also perform well in estimating the free energy contribution of the binding affinity due to the interactions inside proteins (see \cref{ch:binding}).\newline
In this chapter we try to test the ability of \gls{kbp} to capture even more detailed information about protein structures and interactions. With this in mind we investigated some of the limitations of the standard implementation of \gls{bach} (described in \cref{sec:lessons_learned}) and tried to overcome them in two different \gls{kbp} implementations (presented in \cref{sec:moiety,sec:gcp}).

\section{Lessons learned}\label{sec:lessons_learned}
In applying the \gls{bach} scoring function in the contexts of native state recognition and protein binding we investigated the behavior of this simple potential in facing the many situations we put in front of him. One of the strong points of \gls{bach} is its simplicity and reduced number of parameters: it is often very easy to locate the problem when it does not work; as a testing ground, \gls{bach} has proven to be educative.

\subsection{Untold hypotheses}
In order to understand what are the approximations implicitly made in \gls{bach} formulation we tried to derive an equation similar to \cref{statistical_potential_intro} starting from a very simple physical model of two interacting particles.\newline
We assume that the theoretical probability $\hat{p}_{\alpha,\beta}^\ell$ of observing two different particles of type $\alpha$ and $\beta$ respectively in a contact of type $\ell$ is 
\begin{equation}
\hat{p}_{\alpha,\beta}^\ell = \frac{q_{\alpha,\beta}^\ell\exp\left(-\frac{\epsilon_{\alpha,\beta}^\ell}{K_BT}\right)}{\sum_{\ell^\prime}q_{\alpha,\beta}^{\ell^\prime}\exp\left(-\frac{\epsilon_{\alpha,\beta}^{\ell^\prime}}{K_BT}\right)},
\end{equation}  
where $\epsilon_{\alpha,\beta}^\ell$ is an interaction energy, $K_B$ the Boltzmann's constant and $T$ the temperature. Parameters $q_{\alpha,\beta}^\ell$ are intended to be the contact probability between the particle if the energies were zero or, equivalently, if the temperature was infinite. Being a probability, $\sum_\ell q_{\alpha,\beta}^\ell = 1$.\newline
We now try to find the best values for the energy parameters that are compatible with our experimentally observed number of particles of type $\alpha$ and $\beta$ in contact class $\ell$, denoted by $\{ n_{\alpha,\beta}^\ell \}$. If we also assume that the probability of observing the data $\{ n_{\alpha,\beta}^\ell \}$ given the value of energies $\{ \epsilon_{\alpha,\beta}^\ell \}$ is the multinomial distribution
\begin{equation}\label{eq:multinomial}
p\left(\{n_{\alpha,\beta}^\ell\}|\{\epsilon_{\alpha,\beta}^\ell\}\right) = \frac{\left( \sum_\ell n_{\alpha,\beta}^\ell \right)!}{\prod_\ell n_{\alpha,\beta}^\ell !}\prod_\ell \left(\hat{p}_{\alpha,\beta}^\ell\right)^{n_{\alpha,\beta}^\ell}
\end{equation}
we can easily estimate the best values of energies by maximizing the log-likelihood $\mathcal{L}$
\begin{equation}
\mathcal{L} = \sum_{\alpha,\beta}\sum_\ell \log\left[ p\left(\{n_{\alpha,\beta}^\ell\}|\{\epsilon_{\alpha,\beta}^\ell\}\right) \right]
\end{equation}
with respect to the parameters $\epsilon_{\alpha,\beta}^\ell$. As a result we obtain 
\begin{equation}\label{eq:max_likelihood}
\frac{q_{\alpha,\beta}^\ell\exp\left(-\frac{\epsilon_{\alpha,\beta}^\ell}{K_BT}\right)}{\sum_{\ell^\prime}q_{\alpha,\beta}^{\ell^\prime}\exp\left(-\frac{\epsilon_{\alpha,\beta}^{\ell^\prime}}{K_BT}\right)} = \frac{n_{\alpha,\beta}^\ell}{\sum_{\ell^\prime}n_{\alpha,\beta}^{\ell^\prime}},
\end{equation}
where of course the ratio $p_{\alpha,\beta}^\ell = \frac{n_{\alpha,\beta}^\ell}{\sum_{\ell^\prime}n_{\alpha,\beta}^{\ell^\prime}}$ represents our best estimation of the theoretical probability $\hat{p}_{\alpha,\beta}^\ell$. In order to exactly estimate the energy parameters we define a reference class $\ell=0$ and divide both the l.h.s. and the r.h.s. of \cref{eq:max_likelihood} by $p_{\alpha,\beta}^0$ to obtain
\begin{equation}\label{eq:parameter_exact}
\epsilon_{\alpha,\beta}^\ell = \epsilon_{\alpha,\beta}^0 - K_BT\log\left[ \frac{p_{\alpha,\beta}^\ell\; q_{\alpha,\beta}^0}{p_{\alpha,\beta}^0\;q_{\alpha,\beta}^\ell} \right],
\end{equation}
that is the same as \cref{statistical_potential_intro} once we identify 
\begin{equation}
\tilde{p}_{\alpha,\beta}^\ell = \frac{p_{\alpha,\beta}^0\;q_{\alpha,\beta}^\ell}{q_{\alpha,\beta}^0\exp\left(-\frac{\epsilon_{\alpha,\beta}^0}{K_BT}\right)}
\end{equation} 
as the reference probability.

This model is very instructive because it makes explicit the hypotheses that have been necessary to obtain a \gls{kbp}-like formula from a simple physical model.\newline
The parameters $q_{\alpha,\beta}^\ell$ have been supposed to only depend on the type of residues $\alpha$ and $\beta$ and on  the interaction class $\ell$. This assumption seems to be poor, as we already know from \cref{fig:trifonov_peak} that the probability contact of two particles depends on their distance along the polymer chain. Introducing a dependence on the inter-particle distance in $q$ can be easily done but, nonetheless, \gls{bach} does not consider this dependence in its parameters.\newline
Even if \cref{eq:multinomial} seems a reasonable assumption for describing the probability of observing the data $\{ n_{\alpha,\beta}^\ell \}$, the multinomial distribution is the probability distribution of a certain number of \emph{independent} Bernoulli trials, with the same probability of success on each trial. Employing \cref{eq:multinomial} is therefore equivalent to assume that there are no correlations inside the chain. 
But the presence of secondary structures induces high correlations; for instance \cref{fig:beta_correlations} shows the probability density for the distance between two alpha carbons located in two different strands of the same $\beta$-sheet.\newline
Finally, \cref{eq:parameter_exact} shows how critical is the definition of the  reference state $\tilde{p}_{\alpha,\beta}^\ell$ to a correct and unbiased estimation of the parameters. It is also clear that, at least in this case, the correct reference state depends on $\alpha$ and $\beta$. On the contrary, the reference state chosen by \gls{bach} is $$\frac{\sum_{\alpha,\beta}n_{\alpha,\beta}^\ell}{\sum_{\alpha,\beta}\sum_{\ell^\prime}n_{\alpha,\beta}^{\ell^\prime}}$$ and therefore does not depend on residue types.
\begin{figure}
\centering
\includegraphics[width=\textwidth]{img/beta_correlations}
\caption{The effect of correlations inside $\beta$ secondary structures. The figures show the probability density for the distance between two alpha carbons located in two different strands of the same $\beta$-sheet; pair of residues that take part to the same $H$-bond are not considered. }
\label{fig:beta_correlations}
\end{figure}

\subsection{The role of correlations}\label{sec:hp}
In order to get some insight on the functioning of the \gls{kbp} we devised a simple experiment. The aim of the experiment is that of showing how the chain topology can affect the estimation of interaction parameters in \glspl{kbp}. We therefore consider an ideal case in which we have a perfect knowledge on the system (for instance we know the \emph{Hamiltonian} and we also have access to every possible protein sequence and protein structure) and we ask what happens if we try to compute the parameters of a \gls{kbp} by using \cref{statistical_potential_intro}. 
With this in mind we studied a revisited version of the famous $HP$-model on a bi-dimensional square lattice $\mathcal{Z}^2$ \cite{dill1985theory,dill1990dominant}. This approach is similar in spirit to the one in Ref.~\cite{tiana2004deriving}.\newline

In the context of on-lattice protein models each site of the lattice is seen as connected to its adjacent sites: each site can host a protein residue only if it is not occupied by another residue and if the preceding residue is hosted on one of the adjacent sites. As a consequence a protein appears as a self avoiding polygonal chain, briefly referred as \gls{saw}, connecting adjacent sites of a regular lattice. Two sites are said to be \emph{adjacent} to each other (or \emph{nearest-neighbors}) if they lie on the same side of one square defined by the lattice; they are said to be \emph{next-nearest-neighbors} if the lie on the same diagonal of a square. In the case of a square lattice each site has exactly $4$ adjacent sites and other $4$ nearest-neighbor sites.\newline
As in the original $HP$ model each residue can be polar ($P$) or hydrophobic ($H$). The pairwise potential $V_{\alpha,\beta}(r)$ is attractive in case two $H$ residues are nearest-neighbors (but not consecutive along the chain), repulsive if two $H$ residues are next-nearest-neighbors and zero otherwise. \Cref{table:hp_ham} lists all parameters that define the energy of the system.\newline
\begin{table}
\centering
\begin{tabular}{cccc}
\toprule\\
Residue pair & Potential nn & Potential nnn & Potential nc \\
\midrule\\
$PP$ & $0.$ & $0.$ & $0.$\\
$PH$ & $0.$ & $0.$ & $0.$\\
$HH$ & $-1.$ & $\frac{\pi}{17}$ & $0.$\\
\bottomrule
\end{tabular}
\caption{Parameters describing the interactions between pair of residues in the modified version of $HP$ model studied. \emph{Potential nn} refers to the case in which two residues are nearest-neighbors while \emph{Potential nnn} to the case two residues are next-nearest-neighbors. Finally \emph{Potential nc} describe the interaction between all other pairs that are not consecutive along the chain.}
\label{table:hp_ham}
\end{table}
In order to have access to all possible information about the system we restrict the \gls{saw} to have a fixed length $N=16$.
Indeed in this case we are able to enumerate easily all the possible sequences as well as all the possible polymer configurations; moreover we are able to associate an energy to every possible combination of structure-sequence: we therefore have access to the whole universe of proteins.\newline
We defined the native state of a protein its minimum energy configuration \emph{if} this configuration is unique; notice that with this definition not every sequence has a native configuration. In this context the ensamble of native structures is intended to mimic a database of experimentally resolved structures and we indeed employed it to determined the parameters of a \gls{bach}-like \gls{kbp} on the lattice. We suppose that the definition of the interacting classes of this hypothetical potential perfectly matches that of the real interactions in such a way that any difference between the parameters of the \gls{kbp} and that of the real interaction must be solely attribute to the effects of correlations and/or to the choice of the reference state.\newline
\Cref{table:bach_ham} shows the parameters obtained by the described procedure: it is evident how the estimated potential is completely different from the real one. In particular we see how the nearest-neighbor interaction between a polar and an hydrophobic residue appears to extremely repulsive while the repulsive next-nearest-neighbor interaction between two hydrophobic residues appears to be attractive.  
\begin{table}
\centering
\begin{tabular}{cccc}
\toprule\\
Residue pair & Potential nn & Potential nnn & Potential nc \\
\midrule\\
$PP$ & $0.7521863$ & $0.5344563$ & $-0.1378130$\\
$PH$ & $2.5862754$ & $0.0459283$ & $-0.0954614$\\
$HH$ & $-0.95$ & $-0.2416561$ & $0.2454912$\\
\bottomrule
\end{tabular}
\caption{The same as in \cref{table:hp_ham} but here the interaction parameters have been computed with a \gls{bach}-like algorithm.}
\label{table:bach_ham}
\end{table}

This example teaches us that it is possible that correlations completely alter and bias the estimation of the interacting parameters in \glspl{kbp}. We can not know if the effects in real life scenarios are as drastic as in this simple model but nonetheless it is important to be aware of this possibility.
\subsection{Interfaces}
As seen in the previous section, interactions between residues located at the interface between two different monomers are critical in estimating their binding affinity and deserve to be discussed in more details.\newline
The term \emph{interface} between two macromolecules is somehow vague and intuitively define the space in which the two monomers can be consider to be in contact. Therefore we coherently classify a residue as being located on the interface if it interact with at least another residue belonging to the other monomer. From simple geometric consideration we can see that, in the case of compact proteins, while the number of residues scales with the third power of the protein size, the number of residues that belong to the interface scales linearly with it. As a consequence the interface is populated by a relatively low fraction of residues that, nonetheless, play a fundamental role in the binding process. It is not difficult to see that inaccurate classification of these interactions during the protein scoring and/or the usage of inaccurate scoring functions can easily result in wrong estimation of the binding affinity.

We verified that, in some cases in which \gls{bach} completely failed in estimating the binding, affinity the interactions that were taking place at the interface occur very rarely in the protein bulk, where \gls{bach} parameters are computed. 
In retrospect it is not surprising that the effective pairwise interactions depend on the environment (protein bulk or protein surface). Indeed, while in the protein bulk contacts are stabilized by pairwise interactions and by topological constraints, this is not true on surfaces were not only topological constraints are different but also there are entropic effects due to the presence of the solvent.
The first attempt that has been made in order to capture this behavior is that of splitting the side-chain side-chain class of interaction into two different classes that distinguish between non-polar - non-polar interactions and non specific interactions \cite{sarti2015}. The resulting \gls{kbp}, hereafter referred as \gls{ssbach}, has proven to be of fundamental importance in the prediction of binding affinities.


\section{Moieties}\label{sec:moiety}
The poor performance of \gls{bach}, with respect with its improved six-classes version, in the study of binding processes suggests that the coarse grained made in considering each residue as a whole is probably inaccurate.\newline
We therefore studied a statistical potential in which the interacting units are chemical moieties with a more defined chemical behavior. Using such units is important: indeed if two groups of atoms $\alpha$ and $\beta$ have a well defined behavior we can expect that the event ``$\alpha$ and $\beta$ are in interacting class $\ell$'' can be considered as favored or disfavored with a certain confidence and independently on their mutual orientation. This is not true if large set of atoms (such as residues) are employed as interacting units as two units can expose to the other different functional groups, depending on their mutual orientation. The first four classes of \gls{bach} and the two upgraded classes in \gls{ssbach} are devised to account this issue. 

\begin{table}
\centering
\begin{tabular}{lll}
\toprule\\
ID & Description & Formula \\
\midrule\\
ALC & Hydroxyl &  $ROH$\\
CH2 & & $RC(H_2)R^\prime$\\
CH3 & & $RCH_3$ \\
CXY & Carboxyl or Carboxylate & $RC(=O)OH$ or $RC(=O)O^-$\\
SC3 & Methionine functional group & $SCH_3$ \\
THI & Thiol & $RSH$\\
ARG & Arginine functional group & $RN(H)C(=NH)NH_2$ \\
PRK & Group $NH$ in primary ketimine & $RN(H)R^\prime$ \\ 
PAM & Primary amine & $RNH_2$ \\
PRA & Protonated amine & $RNH_3$ \\
PHE & Phenil & $RC_6H_5$ \\
IMD & Imidazole ring in Arginine & $n1c(H)[nH]c(H)c1$ \\
TRP & Tryptophan rings & $c1ccc2c(c1)c(c[nH]2)CR$ \\
PEP & Peptide group & $RC(=O)N(H)R^\prime$\\
\bottomrule
\end{tabular}
\caption{List of moieties employed in the definition of \gls{mbach}.}
\label{table:moiety_list}
\end{table}

We identified $14$ groups of atoms that satisfy the following properties:
\begin{enumerate}
\item have a well defined chemical behavior
\item cover all possible protein structures, without overlapping
\end{enumerate}
Following the evidences that interactions can be different on the interfaces with respect to the protein bulk, we changed the functional form of \gls{bach} in such a way that pairwise interactions between moieties are environment-dependent. This is achieved by distinguishing between four cases:
\begin{itemize}
\item[\textbf{00 -}] Both the moieties are buried
\item[\textbf{01 -}] The first moiety ($\alpha$) is buried while the other ($\beta$) is exposed to the solvent
\item[\textbf{10 -}] The first moiety ($\alpha$) is exposed to the solvent while the other ($\beta$) is buried
\item[\textbf{11 -}] Both moieties are exposed to the solvent
\end{itemize}
Besides these four interacting classes we consider the non-interacting (\textbf{nc}) class, for a total of five pair-wise interacting cases. Notice that the information stored in this classes is redundant as $\epsilon_{\alpha,\beta}^{\mathbf{01}} = \epsilon_{\beta,\alpha}^{\mathbf{10}}$, $\epsilon_{\alpha,\beta}^{\mathbf{11}} = \epsilon_{\beta,\alpha}^{\mathbf{11}}$, $\epsilon_{\alpha,\beta}^{\mathbf{}} = \epsilon_{\beta,\alpha}^{\mathbf{00}}$ and $\epsilon_{\alpha,\beta}^{\mathbf{nc}} = \epsilon_{\beta,\alpha}^{\mathbf{nc}}$. \newline 
We denote this upgraded version of \gls{bach} as \gls{mbach}, where \emph{m} resembles the fact that the interacting units are moieties, rather than residues.

The score in \gls{mbach} is computed as
\begin{equation}
S_{mBACH} = \frac{1}{2}\sum_{i\neq j}\epsilon_{a_i,a_j}^{\ell},
\end{equation}
where the summation in the r.h.s. is extended over all the moieties of the structure and $\ell$ labels the pair-wise interacting class ($\ell \in \{ 00,01,10,11,nc \}$). Parameters $\epsilon_{a_i,a_j}^{\ell}$ are computed as usual by applying \cref{statistical_potential_intro} to the counting obtained by analyzing a large database of high-resolution protein structures \gls{top500}. As in \gls{bach} the reference state is chosen as the average interacting probability and is computed on the same set of protein structure. 
Each moiety is checked to being exposed to the solvent or buried by using the alpha shape based algorithm described in \cref{sec:alpha_shape}: a moiety containing at least one heavy atom that belongs to the alpha shape of the protein is considered exposed to the solvent. The total number of parameters needed in \gls{mbach} is $490$, less than a half of the ones employed by the standard version of \gls{bach}.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/rank_moieties_comp}
\caption{Comparison of the performances of \gls{mbach} with other state-of-the-art methods.}
\label{rank_moieties}
\end{figure}

The usage of moieties as atomic units in the development of \gls{mbach} allows our statistical potential to be applied not only on protein structures but also on other molecules such as drugs and ligands. This potentially interesting application is complicated by the need of automatically recognize functional groups as part of larger molecular structures.  
The drawback arises from  the fact that different nomenclatures are used for labeling atoms in \gls{pdb} files and from the fact that important structural information (such as  bound valency and aromaticity) are not described in \gls{pdb} files at all.
Even if all information that are potentially useful to recognize a moiety as par of a larger molecule can be extracted from the list of atom properties ( for instance bonds can be assigned by comparing the inter-atomic distance with the vdW radii of the two atoms) this approach is not efficient nor reliable. Fortunately the Protein Data Bank provide connectivity information, together with additional atom properties, in separate \gls{mmcif} files.\newline 
We used these information in order to build an undirected graph that describe the structure of the target moleculeand in which vertexes represent atoms while edges represent inter-atomic bonds. Each vertex is described by two properties: an \emph{element} descriptor, which uniquely identify the atom element, and a boolean \emph{aromaticity} flag that classify the atom as aromatic or not aromatic.
Bonds can be both single or double and aromatic or not aromatic, as described respectively by the \emph{type} and \emph{aromaticity} bond descriptors. Once the exact structure topology is built, moieties can be recognized by checking for the existence of some sub-graph isomorphism between the topology of the structure and that of the moiety. A sub-graph of the structure topology $S$ is said to be isomorphic to the graph that describe the moiety $M$ if and only if there is a bijection $f$ from a subset of vertexes of $S$ to the vertexes of $M$ such that, for every pair of vertexes $u$,$v$: 
\begin{enumerate}
\item $f(v)$ and $f(u)$ are connected in $M$ if and only if $u$ is connected to $v$ in $S$
\item each vertex $f(u)$ has the same \emph{element} and \emph{aromaticity} of $u$
\item the bond connecting $f(u)$ to $f(v)$ has the same \emph{type} and \emph{aromaticity} as the respective bond between $u$ and $v$.
\end{enumerate}
Since we require that every atom belongs to at most one moiety, we have to pay attention to the order in which we assign an atom to a moiety. Consider, for instance the two moiety $CH2$ and $CH3$: it is clear that since there are three possible different sub-graph isomorphism between the two functional groups, an atom that belongs to a $CH3$ functional group can also be assigned to a $CH2$ one. The problem is simply overcame by checking the existence of isomorphisms starting from the biggest moiety (\ie the one with the highest number of atoms ) and by proceeding with order until a sub-graph isomorphism between the structure and the smaller moiety is checked. We optimized the process by removing from the large structure those vertexes that have been already assigned to a moiety, in such a way that subsequent steps of the algorithm proceed faster due to the ever decreasing size of the structural graph.

\section{A gaussian chain potential}\label{sec:gcp}
Simplifications usually made in the formulation of knowledge based potentials like \gls{bach} are based on the assumption that the presence of contacts between two groups of atoms inside a macro-molecule is due only to their properties and not to topological reasons. For instance the fact that residues are serially linked together is often neglected and events like ``Residue $i$ is in contact with residue $j$'' and ``Residue $i'$ is in contact with residue $j'$'' are considered as independent.
This is obviously not true, as we already know that, inside $\beta$-sheets, the linear topology of the protein structure induces topological contacts between residues that are not related to any physical interaction (see \cref{fig:beta_correlations}).
But the biggest effects in the estimation of pairwise parameters is probably due to the fact that we forget about the dependence of the distance between two group of atoms on their separation along the chain: it is indeed clear that the contact probability between two residues is a decreasing function of their separation along the chain, as shown in  \cref{fig:trifonov_peak}.\newline
Another limitation of the \gls{kbp} is the adoption of an \textit{a priori} functional form of the potential which is implicitly determined by the set of events used to classify different interaction: its choice is often based of physical/chemical considerations and is not deduced from experimental data. 

The effects introduced by these simplifications are not easily quantifiable; hopefully the reference state can be tuned to partly correct the error induced in the estimation of the parameters but nonetheless neglecting to model a key aspect of the protein structure such as it linear topology has consequences in the estimation of parameters in knowledge-based potentials (see \cref{sec:hp}).\newline

We here propose a new knowledge-based potential that is ideally unaffected by the presence of topological contacts and that keep into account the dependence of the pairwise distance between residues on their separation along the chain. Moreover the method is devised is such a way that the dependence of the score on the pairwise distance $r$ is not imposed \textit{a priori} but is instead  directly recovered from experimental data.

In 2005 Banavar et al. \cite{banavar2005proteins} showed that the end-to-end distances of $m$-residues-long protein fragments are gaussian distributed according to the law
\begin{equation}\label{flory_gauss}
\tilde{P}_\sigma(r,m) = \frac{4\pi r^2}{\left(\frac{2}{3}\pi\sigma^2 m\right)^{\frac{3}{2}}}\exp\left(\frac{3r^2}{2\pi\sigma^2 m}\right),
\end{equation}
with the assumption that $m$ is sufficiently high ($m\geq48$) to \textit{forget} the presence of secondary structures and sufficiently low ($m<N^{\frac{2}{3}}$) to \textit{forget} the presence of the protein surface (as indicated in \cite{lua2004fractal}). This scaling is the one predicted by Flory's theory at the $\theta$ point \cite{flory1969statistical,bhattacharjee2013flory}, for a polymer melt \cite{lua2004fractal}, and, of course, by a gaussian chain model with average bond length $\sigma$. The existence of the gaussian scaling of \cref{flory_gauss} is usually explained by a sort of compensation between the inter-atomic repulsion inside the chain that tend to swollen it and the idrophobic effects due to the presence of the solvent, that tend to compact the protein. As a consequence the end-to-end distance of real-protein fragments behave \emph{as if} fragments were ideal gaussian chains and is ideally unaffected by interactions between residues located in the central part of the fragment, or by other topological effects. \newline
We reproduced the results presented in \cite{banavar2005proteins} by using a larger data-set of globular proteins, selected from \gls{top8000} in such a way that their gyration radius were proportional to the cube root of the chain length (see \cref{fig:gyration}).
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/pot_flory_compact_gyration}
\caption{Distribution of the gyration radius on the selected subset of \gls{top8000}. The slope of the fitted line (in log-log-scale) is $b=0.359\pm0.002$ \AA\; while the intercept is $a=2.493\pm0.001$ \AA. }
\label{fig:gyration}
\end{figure}
We computed the end-to-end distance distribution of protein fragments of length $N^{\frac{2}{3}}$, where $N$ is the total chain length. We also verified that the condition $m\geq48$, suggested in the original paper, can be safely relaxed to $m\geq35$.
\Cref{fig:flory_gaussian_distr} shows the distribution of experimental data obtained from the structures of \gls{top8000} database. End-to-end distances have been computed as the distance between the terminal C$_\alpha$ carbons and have been divided by a factor $\sqrt{m}$ ($x$ axis); $y$ axis has instead been rescaled by a factor $\sqrt{m}$ in such a way that data collapse on the curve of equation
\begin{equation}\label{eq:flory_collapse}
y(x) = \frac{4\pi x^2}{\left(\frac{2}{3}\pi\sigma^2\right)^{\frac{3}{2}}}\exp\left(\frac{3x^2}{2\pi\sigma^2 }\right),
\end{equation}
which is independent from $m$. The variance $\sigma$ of the experimental distribution is $\sigma^\star=3.84\pm0.01$ \AA, a value surprisingly similar to the distance between two consecutive $\alpha$ carbons. 
For each experimental data $(x_i,m_i)$ collected we estimated the corresponding value of probability density by considering the number of data occurring in the interval of width $\Delta=0.01$ \AA\; and centered in $(x_i,m_i)$, \ie $\left[(x_i-\frac{\Delta}{2},m_i),(x_i+\frac{\Delta}{2},m_i)\right]$.\newline
Despite the overall good agreement, experimental data substantially differ from the theoretical distribution in the short-distance regime, where the experimental probability is lower than the theoretical one, and in the long-range regime where, on the contrary, the experimental probability is slightly higher than the theoretical one. 
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/pot_flory_reference}
\caption{Distribution of experimental data (red point) as compared to the expected curve \cref{eq:flory_collapse} and $\sigma=\sigma^\star$. Axis of ordinates is in log-scale.}
\label{fig:flory_gaussian_distr}
\end{figure}

We assume that the observed distribution of end-to-end distances $P(r,m)$ is given by
\begin{equation}\label{eq:gcp_model}
P(r,m) = \frac{\exp\left(-\frac{V(r)}{K_BT}\right)\;\tilde{P}_{\sigma^\star}(r,m)}{\int_0^\infty dr\; \exp\left(-\frac{V(r)}{K_BT}\right)\;\tilde{P}_{\sigma^\star}(r,m)}.
\end{equation}
Indeed the overall good agreement of the theoretical curve in \cref{flory_gauss} with $\sigma=\sigma^\star$ with experimental data allow us to identify \cref{flory_gauss} as the \emph{expected} probability of finding a contact between residues given their separation along the chain and to use it as \emph{reference state} in a \gls{kbp}. 
According to \cref{statistical_potential_intro} we therefore compute a pairwise statistical potential that is a (continuous) function of the distance between $\alpha$-carbon atoms by computing
\begin{equation}\label{flory_pot}
\epsilon(r,m) = -K_BT\;\log\left(\frac{P(r,m)}{\tilde{P}_{\sigma^\star}(r,m)}\right).
\end{equation}
From \cref{eq:gcp_model} we can reduce the previous equation to 
\begin{equation}\label{flory_pot_expl}
\epsilon(r,m) = V(r) + K_BT\;\log\left(\int_0^\infty dr\; \exp\left(-\frac{V(r)}{K_BT}\right)\;\tilde{P}_{\sigma^\star}(r,m)\right),
\end{equation}
which assures us that the estimated function $\epsilon(r)$ differs from the \emph{real} inter-particle potential by only a constant.\newline
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/pot_flory_ALL_ALL_nocorr}
\caption{Average potential between two arbitrary residues inside a globular structure. The colored shadow represent the error associated to the average curve.}
\label{fig:gcp_effective_corr}
\end{figure}
\Cref{fig:gcp_effective_corr} shows an ensamble of $150000$ experimental values of $\epsilon(r,m)$ computed as described above; all points collapse on a well defined curve, independently from their value of $m$. This fact allows us to identify $\epsilon(r) \equiv \epsilon(r,m)$ as an effective potential. We therefore estimated $\epsilon(r)$ by averaging the data obtained from different values of $m$: the curve superposed to the data is a running average of the points and defines the function $\epsilon(r)$. At this stage, we intentionally neglect the dependence of the potential on the type of the interacting residues: the curve depicted in \cref{fig:gcp_effective_corr} is an average effective-potential that do not distinguish between different type of amino acids.\newline
\begin{figure}
        \centering
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth, height=0.2\textheight]{img/pot_flory_ALA_ALA_nocorr.png}
                \caption{}
                \label{flory_pot_ala_ala}
        \end{subfigure}
        ~ 
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth, height=0.2\textheight]{img/pot_flory_VAL_VAL_nocorr.png}
                \caption{}
                \label{flory_pot_val_val}
        \end{subfigure}
        
        ~ \\
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth, height=0.2\textheight]{img/pot_flory_ARG_ARG_nocorr.png}
                \caption{}
                \label{flory_pot_arg_arg}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth, height=0.2\textheight]{img/pot_flory_ILE_ILE_nocorr.png}
                \caption{}
                \label{flory_pot_ile_ile}
        \end{subfigure}
        
        ~ \\
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth, height=0.2\textheight]{img/pot_flory_ALL_ALL_nocorr.png}
                \caption{}
                \label{flory_pot_all_all}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth, height=0.2\textheight]{img/pot_flory_compare_nocorr.png}
                \caption{}
                \label{fig:flory_pot_compare}
        \end{subfigure}        
        \caption{A selection of pairwise potentials. Panel \ref{flory_pot_ala_ala} describe the interaction between two Alanine residues, panel \ref{flory_pot_val_val} the one between two valine residues, panel \ref{flory_pot_arg_arg} the one between two arginine residues and finally panel \ref{flory_pot_ile_ile} depicts the interaction between two isoleucine residues. Panel \ref{flory_pot_all_all} represents the effective interaction between residues, independently from their type and panel \ref{fig:flory_pot_compare} shows a direct comparison of all the potentials shown in the other panels. In all figures solid dots are the running average of the data while the shaded region is the error associated with the average in each point: its width is twice the standard deviation of the set of data that contribute to the running average. }\label{flory_pot_panel_nocorr}
\end{figure}
Data suggest that proteins can be described as repulsive chains with an hard-sphere repulsion (for distances $r\lesssim4$ \AA) and a soft shell (for distances $4$ \AA $\lesssim r \lesssim 10$ \AA). Residues feel no interactions, on average, for distances greater than $10$ \AA. \newline
We recover the dependence on the type of the interacting residues by restricting the set of fragments used for computing the contact probability $P^{\alpha,\beta}(r,m)$ to the ones whose first and last residues were respectively of type $\alpha,\beta$ of $\beta,\alpha$.	
Beta sheets have been identified as a source of spurious contacts and the effects of these contacts have been canceled. In order to avoid biases in the computation of $P^{\alpha,\beta}(r,m)$ due to spurious contacts inside $\beta$-sheets (see \cref{fig:beta_correlations}) we did not consider pair of C$_\alpha$ carbons that belong to the same $\beta$-sheet and that are not hydrogen-bonded to each other. \newline
Again we used theoretical distribution $\tilde{P}_{\sigma^\star}(r,m)$ as reference state and we compute the pairwise parameters of the scoring function by employing \cref{statistical_potential_intro} as
\begin{equation}
\epsilon^{\alpha,\beta}(r) = -\log\left(\frac{P^{\alpha,\beta}(r,m)}{P_{\sigma^\star}(r,m)}\right)
\end{equation} 
where indexes $\{\alpha,\beta\}$ label the amino acid type and $P^{\alpha,\beta}(r,m)$ represents the end-to-end distance of fragments whose first residue is of type $\alpha$ and the last one is of type $\beta$, or vice-versa. The resulting potentials (some of which are shown in \cref{flory_pot_panel_nocorr}) exhibit features that are peculiar of the residues involved in the interaction: notice for instance how the interaction between two charged ARG is repulsive, as expected. We also notice that the value of $\epsilon(r)$ at large values of the distance is approximately zero. This make us to suppose that the constant $\epsilon(r)-V(r)$ is small when compared to typical values of $\epsilon(r)$.

%The presence of a minimum at a distance value of $6$ \AA in the potentials of those residues that are often found in $\beta$-sheets (as ...) is unexpected, as the typical distance between the two alpha carbon of the interacting residues in a $\beta$-sheet is approximately $5$ \AA. The observed minimum is an artifact caused by topological interactions inside $\beta$-sheet structures, as can be deduced from \cref{}. Indeed, for every H-bonded pair of residues $i,j$ there are two pair of residues at position $i,j\pm1$ (if we exclude the case in which $i,j$ are on the boundary of the sheet), whose distance is roughly $6$ \AA.
%In order to relieve the computation of the potentials from these spurious contacts we simply exclude every pair of residues belonging to the same $\beta$-sheet and that is not H-bonded from the calculation of $P^{\alpha,\beta}(r,m)$. The new potentials we obtain are shown in \cref{}: as can be seen the minimum has moved toward its expected value.

In view of more appropriate choice of the reference state, the \gls{kbp} introduced in this section correctly accounts for the dependence of the contact probability on the separation along the chain. 

Even if the technique described in this section seems promising for the development of reliable \glspl{kbp}, it is necessary to point out how this specific potential is not able to distinguish the different possible pairwise interactions between two residues, \eg does not depend on their relative orientation. As a consequence a benchmark on the same \gls{casp} targets employed in \cite{cossio2012simple} and in \cref{sec:lcpo,sec:alpha_shape} shows its limited performances in recognizing the native structure of proteins (see \cref{fig:rank_gcp}).\newline
We can then conclude that categorize the possible interactions between residues i smore crucial than to take into account the correlations inside the protein chain. Further or going work is planned to develop this strategy.
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/rank_gcp}
\caption{Comparison of the performances of Gaussian Chan Potential (GCP) with other state-of-the-art methods.}
\label{fig:rank_gcp}
\end{figure}