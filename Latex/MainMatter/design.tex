\chapter{Knowledge Based Potentials}\label{ch:kbp}
Since the early 1980's \cite{miyazawa1985estimation}, scientists have been trying to use the ever\hyp{}increasing data availability about protein structure and function to redesign existing proteins, and more recently, to design entirely new proteins.
Many promising designing methods have been proposed in the last decades \cite{degrado1989protein,bryson1995protein,dahiyat1997novo,harbury1998high,kuhlman2003design}.

Protein design consists the creation of novel protein sequences with arbitrarily chosen three\hyp{}dimensional structures \cite{richardson1989novo}.
Given a target structure, the design process can be coarsely divided into two steps.\newline
The first step consist in the proposal of a test sequence $S$ that, during the second step, has to be tested with some suitable and specific method. The testing methods usually takes advantage of the observed regularities exhibited by protein structures in order to assess the quality of the proposed protein. Indeed different types of amino acids, having their own chemical features, are found in different strategic positions of the structure to stabilize the fold. A collection of carbon\hyp{}rich amino acids, like leucine and phenylalanine, are usually placed inside the protein, and lock perfectly together. On the other hand, charged amino acids, such as lysine and aspartic acid, are typically spread across the surface to make the protein soluble in water. Hydrogen\hyp{}bonding amino acids, such as serine and asparagine, are spread in strategic positions to tie different portions of the chain together. Finally, glycine and proline are added to redirect the chain in the proper direction. This combination of favorable forces locks the protein chain into a stable and compact structure.\newline 
The two steps described above are iterated several times and the sequence that better fit the target structure is finally named the \emph{designed} sequence.\newline
Ideally the iteration would happen over every possible sequence having the right length but this is practically impossible: indeed even enumerating all the sequences of a small protein composed of only $30$ residues is unfeasible with modern computers. The solution is restricting the search space to some subset of sequences with desired characteristics such as a reasonable ratio of hydrophobic and hydrophilic residues. The test sequence is often obtained by modifying the sequence of a protein with a structure similar to the target one (protein redesign).

In order to design a protein that will successfully fold the previously described steps are not sufficient and to assure that the protein only has one stable structure is also necessary. Indeed any other fold compete with the desired stable structure thus interfering with it. Therefore designing a stable protein structure  is not enough and the design of a protein structure that is unstable in every other conformation is needed. 

We focused our attention on a class of methods that are most frequently employed in the second stage of protein design and that are referred to as \glspl{kbp}. Despite their popularity and success \cite{kuhlman2003design} in the field of protein design, the application of these methods is also exploited in other areas where the validity and accuracy of a proposed tertiary structure is of fundamental importance, as for instance protein structure prediction and refinement. Indeed many of the several scoring functions that have been proposed for recognizing protein native structures \cite{ferrada2007knowledge,dong2006novel,mcconkey2003discrimination,zhou2002distance,fang2006protein,rajgaria2008distance} are rooted on \glspl{kbp}. Broadly speaking \gls{kbp} are energy functions derived from databases of known protein conformations that empirically aim to capture the key aspects of the physical chemistry of protein structure and function. As benchmarked during the biennial \gls{casp} \cite{moult2009critical}, the performances of \glspl{kbp} in the recognition of native states are reasonably good, but significant room for improvement remains. For the advantage of the scientific community, many of these methods are publicly available through web servers so that users may have direct access to scoring functions for quality estimation of their own protein structural models.

\section{Assess the quality of a structure}
\Glspl{kbp} are potential functions that associate a real number to the three\hyp{}dimensional configuration of a (macro-)molecule. The molecular structure is often represented by mean of a coarse\hyp{}grained model, in which groups of atoms are merged together into a single effective unit or particle. The term \emph{knowledge based} reflects the fact that the parameters modulating the potential are inferred from experimental data, typically from the knowledge of the three\hyp{}dimensional structure of a large number of macromolecules. The aim of \glspl{kbp} is to take advantage of the availability of such data in order to estimate how much a molecular structure  resembles its (unknown) native structure. Since the native structure corresponds to the minimum energy conformation of the protein, \gls{kbp} usually try to describe in an effective and coarse\hyp{}grained fashion all the inter-atomic interactions that stabilize the protein. Similarly to physical potential energies \glspl{kbp} assign a  negative (favorable) score to events that happen more frequently than expected and positive score to rarer events. The expected probability of an event is called \emph{a priori} probability and acts as a reference for distinguishing between favorable and non favorable interactions. The set of all \emph{a priori} probability constitutes the \emph{reference state} of the \gls{kbp}.

All \gls{kbp} are based on a common theoretical framework and are specified by four elements:
\begin{itemize}
\item a base knowledge
\item chain representation
\item event definition
\item reference state
\end{itemize}
The knowledge on which the potential is based is usually constituted by a (preferably large) set of protein structures. The choice of the data-set is therefore fundamental and has to be performed carefully, since different classes of proteins (globular, fibrillar, ...) are expected to exhibit different effective interactions. A good \gls{kbp} must be at least robust over the change of starting data-set, \ie the parameters estimated from two different (but equivalent) data-set of protein structures must be the same within the errors.\newline
The macro\hyp{}molecule representation is at the core of the \gls{kbp} formulation. Indeed particles used to represent the structure constitute the interacting units upon which the potential will act. In case the macromolecule is a protein the natural choice for such units are the residues themselves but other choices, such as functional groups or single atoms, are equally appropriate.\newline
The interactions between structural units are usually defined in a coarse-grained way themselves, by choosing a set of events to be associated to the interactions. Typical events are \textit{``A particle of type $\alpha$ and particle of type $\beta$ are found at a distance $r\pm\Delta r$''} or \textit{``A particle of type $\alpha$ and particle of type $\beta$ are found at a distance $r < r_0$''} or again \textit{``A particle of type $\alpha$ and particle of type $\beta$ are hydrogen-bonded''}, but infinite other definitions are possible.  Since the set of events is often chosen to mimic different interaction that can take place between pair of particles, we introduce the concept of \emph{interacting class} in such a way that each event can be described by the sentence \textit{``The interaction between particle of type $\alpha$ and particle of type $\beta$ belong to interaction class $\ell$''}. Each event can therefore be identified by three indexes: the first two ($\alpha$ and $\beta$) identify the pair of particles while the last one ($\ell$) identifies the kind of interaction that occurs between the two. It is important to notice that the choice of the interacting classes is equivalent to imposing a functional form to the potential. 
The definition of the interacting classes is in general not \textit{knowledge based} but motivated by physical or chemical reasoning.\newline 
Notably an intimately dependence of the definition of the interacting classes on the coarse-grained level used for describing the structure exists. For instance if particles are composed of a large group of atoms different relative orientations of two particles could in principle lead to different physical interactions between them. In addition the loss of details induced by the coarse grained should be integrated by specialize the interaction classes. 

In order to estimate the statistical interaction associated to an event it is necessary to set an expected probability $\tilde{p}_{\alpha,\beta}^{\ell}$ for that event. The choice of this reference probability is highly non trivial and largely discussed in the literature \cite{muegge2002knowledge,zhou2002distance,liu2004physical}. The observed probability $p_{\alpha,\beta}^{\ell}$ is instead computed from the previously selected database. 
The score associated to the event is finally estimated as 
\begin{equation}\label{statistical_potential_intro}
\epsilon_{\alpha,\beta}^{\ell} = -K_BT\cdot \log\left(\frac{p_{\alpha,\beta}^{\ell}}{\tilde{p}_{\alpha,\beta}^{\ell}}\right),
\end{equation}
as proposed in \cite{sippl1990calculation}.

\subsection{Solvent}\label{sec:solvent}
One aspect of the formulation of \glspl{kbp} that we have omitted in the previous section but deserves to be examined is the role of interactions between the protein and the solvent \cite{miyazawa1985estimation}. Indeed even if the folding process has not been understood yet (and is still far from being understood) it is known that protein-solvent interactions are crucial in determining the correct fold of a protein \cite{bolen2001osmophobic,jackson2006molecular}.\newline
From a practical perspective modeling the interactions happening between protein and solvent is fundamentally different from the case of interaction between particles inside the protein structure.\newline
The first challenge arises from the fact that experimental three\hyp{}dimensional coordinates of solvent molecules are usually not present in \gls{pdb} files. In order to overcome this problem, the solvent is often modeled as an homogeneous medium in which the protein is immersed (\emph{implicit solvent}). However, this approach introduces new challenges, due to the continuum nature of the solvent. Indeed, since physical solvent molecules are missing it is not possible to talk about residue-solvent \emph{interaction} but more precisely about the probability that a particle is interacting with the solvent. If this probability is zero we denote the particle as \emph{buried}; otherwise it is \emph{exposed to the solvent}. The degree of exposure of a particle to the solvent is measured by the area in \AA$^2$ of the interface between the solvent and the particle itself. 

We here consider \gls{sasa} as an approximation of the area of this interface.\newline
\begin{figure}
\centering
\includegraphics[width=0.35\textwidth]{img/surface_sasa}
\caption{A graphical sketch of the solvent accessible surface (blue line) of a group of atoms (grey circles) compared to their van der Walls surface (red lines). The water molecule is assumed to roll around atoms: during this motion the center (yellow dot) of the water molecule (blue circle) slips onto the solvent accessible surface.}
\label{fig:sasa}
\end{figure}
The solvent accessible surface is a virtual surface that surround the protein and whose points constitute the center of a spherical probe in its maximum approach position to the protein (see \cref{fig:sasa}). The value of \gls{sasa} of a given group of atoms therefore constitute a measure of how much that group is exposed to the solvent. 
In last decades both  analytic \cite{fraczkiewicz1998exact,hayryan2005new,connolly1983solvent} and approximated \cite{hasel1988rapid,street1998pairwise} methods for computing \gls{sasa} have been developed. Thanks to their reduced computational time, approximated methods are often preferred on the analytical ones in all those applications (MC, MD) in which \gls{sasa} needs to be computed a large number of times; as usual the choice of the preferred algorithm has to consider the best compromise between reliability and computational efficiency.

As \gls{sasa} is the most meaningful quantity that can be used to measure the degree of exposure of a particle to the solvent, it is not surprising that protein-solvent interaction in \glspl{kbp} are often parameterized by this quantity. 

\subsection{Benchmark}
\Glspl{kbp} are benchmarked by comparing their performances in recognizing the native state of a protein among a large set on non-native structures. 
During the biennial \Gls{casp} competition hundreds of independent research groups try to predict the structure of a target protein by using state-of-the-art algorithms. The resulting decoys sets, composed by the native structure and by all the proposed models, show high structure similarity and therefore constitute a challenging test for \glspl{kbp} \cite{handl2009artefacts}.
Although it is not trivial to definitely assess their absolute efficiency, many \gls{kbp} perform quite well: Rosetta \cite{simons1999improved,tsai2003improved}, a scoring function derived using an elegant Bayesian analysis, the composite scoring function QMEAN6 \cite{benkert2008qmean} and the potential RF\_CB\_SRS\_OD introduced by Rykunov and Fiser \cite{rykunov2010new} are particularly successful, even when tested on \gls{casp} targets.

\section{BACH}\label{sec:bach}
In 2011 Cossio et al.  developed a \gls{kbp} called \gls{bach} \cite{cossio2012simple}. 
This potential is constructed by analyzing a set of 500 experimentally resolved protein structures from \gls{top500} database, monitoring the probability of the event in which single residues or residue pairs are observed in different structural classes.\newline
The \gls{bach} score function is obtained by adding together two contributions that account for residue-residue as well as for residue-solvent interactions. The residue-residue term is often denoted as \emph{pair} contribution while the residue-solvent term is addressed as \emph{solvation} term. The pair score is weighted with a positive parameter $p$  that fixes the relative units of the two contributions:
\begin{equation}\label{eq:bach_formulation}
S_{BACH} = p\times S_{PAIR} +  S_{SOLV}.
\end{equation}
The pairwise statistical potential is based on classifying all residue pairs within a protein structure in five different structural classes, labeled by integers from $1$ to $5$ in decreasing order of priority. One pair cannot be classified as belonging to one class if it is also classified in a second class with higher priority (lower label), in such a way that each pair only belongs to one class. Two residues may form a $\alpha$-helical bridge (1), or an anti-parallel $\beta$-bridge (2), or a parallel $\beta$-bridge (3), or may be in contact with each other through side chain atoms (4), or may not realize any of the previous four conditions (5). 
A modified version of the \gls{dssp} algorithm \cite{kabsch1983dictionary} is used in order to detect $\alpha$ and $\beta$-bridges. The modified algorithm employs a more stringent energy threshold ($-1\;\si{\kilo\calorie\;\mol^{-1}}$ in place of the original $-0.5\;\si{\kilo\calorie\;\mol^{-1}}$) to assess hydrogen bond formation. Since hydrogen atoms are often missing in experimental data, we used a simple geometrical rule borrowed from \gls{dssp} to reconstruct the position of backbone hydrogen atoms, which are required in the computation of the H-bridge energy.
A residue pair is assigned to the side chain-side chain contact class if any inter-residue pair of side chain heavy atoms is found at a distance lower than $4.5$ \AA. If a pair of residues is not assigned to any class labeled with $\ell<5$ it is automatically assigned to the non-interacting class.\newline
Pairwise parameters can be stored in five symmetric matrices $\bar{\epsilon}^\ell$ whose entries $\epsilon_{\alpha,\beta}^{\ell}$ represent the score assigned to a pair of residue of given type $\alpha$ and $\beta$, whose interaction is classified as class $\ell$.
The number of parameters required by the pair-wise potential, once the symmetry $\epsilon_{\alpha,\beta}^\ell = \epsilon_{\beta,\alpha}^\ell$ is kept into account, are $1050$.\newline
The pairwise contribution to \gls{bach} statistical potential $S_{PAIR}$ is computed as
\begin{equation}
S_{PAIR} = \sum_{i<j}\epsilon_{a_i,a_j}^{\ell},
\end{equation}
where $a_i,a_j$ represent the type of amino acid in position $i$ and $j$ along the chain, and $\ell$ identifies the contact class between residues $i$ and $j$.
Notice that \gls{bach} does not score the structure of a protein but rather its contact map: small perturbations of the structure that do not affect its contact map do not affect the value of $S_{PAIR}$ either.

Similarly, the solvation term is based on classifying all residues in two different environmental classes, either buried or solvent exposed. The environmental class is defined based on the evaluation of the \gls{sasa} performed by the \gls{surf} tool of \gls{vmd} graphic software. The \gls{sasa} is computed by \gls{surf} for all heavy atoms of the protein chain by rolling a probe sphere (representing a water molecule) on the surface of the set of spheres centered at heavy atom coordinates. The radii of the probe sphere and of all atoms are set to $1.8$ \AA. The radius of water is higher than what is employed in \gls{vmd} ($1.4$ \AA) in order to avoid considering internal cavities as areas exposed to the solvent. The output of \gls{surf} is the number of triangle vertexes associated to each atom of the protein. These vertexes are used in the triangulated representation of the protein surface employed by \gls{vmd}, and the area associated with each vertex is approximately $ 0.15$ \AA. By summing over all atoms of a given residue, the number of vertexes $t$ associated to that residue (which is approximately proportional to its \gls{sasa}) is obtained. The single residue statistical potential $S_{SOL}$ requires two separate parameter sets, for overall $40$ parameters.

The reference state used for defining \gls{bach} pairwise parameters $\epsilon_{\alpha,\beta}^\ell$ corresponds to the observed probability for a pair of residues to be in interaction class $\ell$, without distinguishing their type $\alpha,\beta$. Similarly the solvation reference state $\tilde{p}_\alpha^\ell$ is simply given by the probability of observing a residue in the environmental class (buried or exposed to the solvent) $\ell$. In both cases the reference state is computed from the same set of proteins used for computing \gls{bach} parameters.

The parameter $p$ introduced in \cref{eq:bach_formulation} is used to tune the contribute of the pairwise scoring function with respect to the solvation part. Its value has been decided in such a way that the standard deviation of the solvation contribution to the scoring of each structure in \gls{top500} is the same as that of the pairwise contribution and results to be $p=0.6$.

Notice that since \gls{top500} is a database of globular proteins, \gls{bach} parameters could be suitable to only describe effective interactions inside globular proteins.

\Cref{fig:rank_bach} shows the performance of \gls{bach} in discriminating the native state of a set of $33$ decoys from \gls{casp} $8$ and \gls{casp} $9$ competitions \cite{moult2009critical}. For each decoy, all structures are ordered in increasing value of \gls{bach} score: the normalized score is computed as the position of the native structure (its rank) divided by the total number of structures in the decoy. The lower the normalized score, the better the performance of the \gls{kbp}. Normalized ranks of all $33$ decoys are subsequently ordered from the lower to the higher, as in \cref{fig:rank_bach}.\newline
\gls{bach} has proved to outperform other state-of-the-art methods in discriminating the native state of proteins among a large set of misfolded configurations of the sequences. 
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/rank_other}
\caption{Performances of different \glspl{kbp} on \gls{casp}8-9 decoys (the lower the better).}
\label{fig:rank_bach}
\end{figure}

Authors also showed how using the mean value of \gls{bach} score obtained during a short molecular dynamics trajectory enhanced the performance of the algorithm in those cases in which the native structure was not correctly recognized. \Gls{bach} seemed to be sensitive to rather small fluctuation of proteins around their native structure: this suggests that fluctuations cannot be neglected in order to correctly determine the native state of a protein.

The simplicity of the model employed in \gls{bach} scoring function and its performances compared to other state-of-the-art knowledge-based-potentials encouraged us to develop the ideas beneath it and to devise other potentially interesting applications in the field of protein bio-physics.

\section{Enhancing BACH}
The modifications we propose to enhance the performances of \gls{bach} are focused on the development of new algorithms for determining the environmental class of each residue. Indeed there are three main disadvantages in the usage of the rolling-ball algorithm implemented in \gls{surf}:
\begin{itemize}
\item the usage of the external routine results in a limited ability to modify the rolling-ball algorithm as needed
\item the computation of \gls{sasa} is not efficient since the routine can not be perfectly integrated into the \gls{bach} source-code
\item the rolling-ball algorithm itself is relatively slow
\end{itemize}
For these reasons we implemented and tested two different algorithms that are able to determining if a group of atoms is buried or exposed to the solvent. \newline
The first algorithm we present estimates \gls{sasa} by using a modified version of the \gls{lcpo} algorithm \cite{weiser1999approximate}. The algorithm presented in \Cref{sec:alpha_shape}, on the contrary, determines if a particle is exposed to the solvent without computing its \gls{sasa} and therefore saving computational time.

\subsection{Linear Combination of Pairwise Overlaps}\label{sec:lcpo}
We here propose to assign a residue to an environmental class by estimating its \gls{sasa} in a simpler manner, following the \gls{lcpo} algorithm presented in \cite{weiser1999approximate}. This approach approximates the accessible surface of a solute as a linear combination of the surfaces of its atoms, modeled as spheres of radius $r$. The working principle is to remove from the sum of the whole surface contribution of each atom the estimated overlap of the surfaces of nearby atoms. The exposed surface of the atom $i$ is approximated as follows:

\begin{eqnarray}
A_{i} & = & P_{1}4\pi r_{i}^{2}+P_{2}\sum_{j\in N(i)} A_{ij} + P_{3}\sum_{\substack{j,k\in N(i) k\in N(j)
k\ne j} }A_{jk}+ \nonumber \\ & &  +  P_{4}\sum_{j\in N(i)}A_{ij}\Bigg(\sum_{\substack{k\in N(i) k\in N(j)
k\ne j} }A_{jk}\Bigg)\:, \label{lcpo}
\end{eqnarray}
where
\begin{equation}
A_{ij} = 2\pi r_{i}\left(r_{i}-\frac{d_{ij}}{2}-\frac{r_{i}^{2}-r_{j}^{2}}{2d_{ij}}\right)\:. \label{lcpo2}
\end{equation}
The quantity $r_{i}$ is the radius of atom $i,$ $N(i)$ stands for the list of atoms that overlap with atom $i$ and $d_{ij}$ is the center-to-center distance of atom $i$ and $j$. In the original work \cite{weiser1999approximate} the four parameters $P_{1}$-$P_{4}$ depended on the hybridization of the atom and on its neighborhood and were estimated by linear regression of a heterogeneous database of analytically calculated cases.

In order to simplify the implementation of the method, we decided to attempt a coarse approximation, using for all the heavy atoms of the protein a single value of the radius $r_{LCPO}$, and single set of parameters $P_{i}$, the one introduced in \cite{weiser1999approximate} for a $sp_3$ carbon bound with three heavy atoms. Since the parameters $P_{2}$ and $P_{3}$ are negative the value of $A_{i}$ can also be negative. By visual inspection, we checked that this normally happens when the atom is deeply buried in the protein core. We therefore assumed that a residue is solvent exposed if the sum of the values $A_{i}$ of its atoms is larger than zero.

The only free parameter in the functional form (\ref{lcpo}) is thus the radius of the heavy atoms. To determine it, we defined a \emph{coherence score} as the fraction of residues over all the \gls{top500} structures for which \gls{surf} and the \gls{lcpo} algorithm agree on the environmental class assignation ($b$ or $e$). We then found the radius parameter that maximizes the value of this score. We reached a maximum $ 87\% $ overall accordance for the value of $r_{LCPO}=3.09$ \AA. This value roughly corresponds to the sum of the Van der Waals radius of an aliphatic carbon ($1.6$ \AA) and the radius of a water molecule ($1.4$ \AA). 
We named this upgraded version of \gls{bach} as \gls{bach++}.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/rank_lcpo}
\caption{\Gls{bach++} benchmark on $33$ decoy sets from \gls{casp} 8/9 competition; the performance is comparable to that of other state-of-the-art methods.}
\label{fig:rank_lcpo}
\end{figure}

We benchmark this functional form of $S_{solv}$ by the same procedure followed in \cite{cossio2012simple}. In particular, we consider a selection of $33$ decoy sets from \gls{casp} 8/9 competition \cite{moult2009critical}. For all the decoy sets we first compute the value of $S_{sol}$ for all the decoy structures and for the native structure. We define the normalized ranking as the rank in the sorted list of the native structure, divided by the number of structures of the decoy set. The smaller this number, the better the solvation function discriminated the native among the decoys.
In \cref{fig:rank_lcpo} the rankings of \gls{bach} and \gls{bach++} are compared to that of other state-of-the-art methods. In \cref{fig:rank_lcpo_solv} the sole contribution of the solvation term is instead considered. We can see that this new \gls{lcpo}-based procedure guarantees a slightly more accurate prediction of the native structure with respect to \gls{bach}.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/rank_lcpo_surf}
\caption{Solvation term contribution to \Gls{bach++}: benchmark on $33$ decoy sets from \gls{casp} 8/9 competition; the performance is comparable to that of \gls{bach}.}
\label{fig:rank_lcpo_solv}
\end{figure}

\subsection{Alpha shapes}\label{sec:alpha_shape}
As seen in \cref{sec:solvent} in order to classify a group of atoms as buried or exposed to the solvent the value of its \gls{sasa} $\mathcal{S}$ is often used . The classification is decided by comparing the surface area to some pre-determined and fixed threshold value $\mathcal{S}^\star$: if $\mathcal{S}$ exceeded $\mathcal{S}^\star$ the group of atoms is considered exposed to the solvent, otherwise it is considered buried.\newline
In this section we will present a technique that greatly enhances this procedure from a computational efficiency perspective and produces, at the same time, a more exact classification.

The main problem in estimating \gls{sasa} is computational time. Indeed existing algorithms (see previous section on \gls{lcpo}) need to iterate on every pair of atoms belonging to the structure and, as a consequence, the time required is proportional to the square of the number of atoms considered. \newline
Fortunately it is not necessary to compute \gls{sasa} of a group of atoms in order to determine if it is located on the protein surface: the information stored in the sentence ``\gls{sasa} is $\mathcal{S}$'' is much more than the information required by \gls{bach}, which can be summarized in the sentence ``Is \gls{sasa} greater than $\mathcal{S}^\star$?''. Computing the surface area requires a great amount of computational resources and time that can be saved by an algorithm devised to compute just the right amount of information needed.\newline
The algorithm we proposed computes the \emph{shape} of the desired protein or protein complex and manage to classify every atom of the structure as belonging to the surface in sub-quadratic time. 

The shape of an object is a vague notion and there are probably many possible interpretations; among these we utilize that of $\alpha$-shape.
The concept of $\alpha$-shape is relatively new \cite{edelsbrunner1983shape} but is nowadays largely used for shape reconstruction in many different fields like computer sensing \cite{fayed2009localised,bernardini1997sampling}, topography \cite{vauhkonen2010imputation}, and biology \cite{binkowski2003castp,tondel2006protein}.
Alpha-shapes are piece-wise linear surfaces made up of triangles.
A triangle is drawn for all triplets in the input set that sit on the boundary of a probe sphere of radius $\alpha^\frac{1}{2}$ that contain no other points in the input set. The radius of the probe sphere plays a fundamental role in the accuracy of the algorithm and need to be decided with meticulous attention: indeed if a too large value is used most of the details of the reconstructed surface would be lost. On the contrary the usage of a probe that is too small if compared to the typical distance between points will result in a disconnected shape. Notice that the alpha shape obtained with a infinitely large probe ($\alpha=\infty$) corresponds to the convex hull of the original set of points. \Cref{fig:alpha_shape} shows pictorially how the reconstructed shape can depend on the choice of probe radius.\newline
\begin{figure}
\centering
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{img/surface_batman_alpha_small}
\caption{Appropriate probe radius.}
\label{fig:alpha_shape_small}
\end{subfigure}
~
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{img/surface_batman_alpha_large}
\caption{Large probe radius}
\label{fig:alpha_shape_large}
\end{subfigure}
\caption{Two different alpha-shapes (black lines) of a random set of points (blue) in the plane. In panel (b) the larger radius of the probe (red circles) is responsible for the loss of details of the reconstructed shape. }
\label{fig:alpha_shape}
\end{figure}
The algorithm for reconstructing the protein surface uses the $\alpha$-shape implementation of the \gls{cgal} library \cite{cgal}. The set of input points that is used for determining the shape of the macromolecule is formed by the position of every heavy atom of the structure; as usual hydrogen atoms are not considered because information about their position is often missing from experimental data. In the context of protein surface reconstruction the probe sphere used by the algorithm mimic the presence of a solvent molecule and therefore its size should reflect the steric effects between (heavy) atoms of the structure and solvent molecules. The solvent is assumed to be water and so we expect the correct radius to be greater than the typical dimension of an $H_2O$ molecule, \ie $1.4$ \AA. \newline
In order to determine the best probe radius value we studied how the classification of residues of a set of $50$ different globular proteins randomly chosen among those of the \gls{top500} database changes as a function of the probe radius.  We considered the \emph{exact} classification to be the one obtained  using an analytical method for computing \gls{sasa} \cite{fraczkiewicz1998exact} and by setting a threshold value $\mathcal{S}^\star=0$. The probe radius was therefore chosen in such a way that the error committed in classifying residues as buried or exposed to the solvent is minimized.
\Cref{fig:alpha_shape_LCPO} shows how the relative error varies as a function of the probe radius: the minimum is obtained for a probe radius of $r_p=3.2\pm0.01$ \AA\; that, as expected, is greater than the typical value used as radius of a water molecule ($r_{H2O}=1.4$ \AA). The larger effective radius of the solvent molecule accounts for the steric effects between the solvent and structure heavy atoms as well for the steric effects due to the presence of hydrogen atoms, that are not considered in the model.

We also compared the error obtained by using the $\alpha$-shape algorithm with the one obtained by using the \gls{lcpo} one.\newline
We recall that \gls{lcpo} algorithm estimates the \gls{sasa} of a group of atoms as a linear combination of terms composed by pairwise overlaps of hard spheres of fixed radius $r_{\gls{lcpo}}$ and centered on heavy atoms. Notice that the sphere used by the \gls{lcpo} algorithm has the same physical interpretation than the probe used in the $\alpha$-shape method, its size having to mimic the steric effects of a water molecule and atoms inside proteins. The same \cref{fig:alpha_shape_LCPO} also shows the relative error obtained in the classification of residues by the modified version of \gls{lcpo} as a function of $r_{\gls{lcpo}}$. We noticed that the minimum error obtained with \gls{lcpo} is \emph{twice} the error produced by the $\alpha$-shape algorithm. Moreover the fact that the width of the minimum obtained with $\alpha$-shape is considerably larger than the one obtained with \gls{lcpo} indicates that the classification is more stable, and therefore reliable, in the former case.

The improved performances of the $\alpha$-shape based algorithm allow the application of the same technique for the classification of even smaller sets of atoms, for which the classification proposed by \gls{lcpo} method becomes unreliable.
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/alpha_shape_LCPO}
\caption{Error committed in the classification of residues as buried or exposed according to \gls{lcpo} (blue line) and $alpha$-shape (red line) algorithms.}
\label{fig:alpha_shape_LCPO}
\end{figure}

The $\alpha$-shape algorithm implemented in \gls{cgal} also computes the tetrahedralization of the space induced by the input points, that is a subdivision of the protein inner space obtained by connecting near atoms, similarly to what is done by the Delaunay tetrahedralization algorithm. This space partition can be used to determine for each atom a list of neighboring atoms: the corresponding graph describing the vicinity relation between input points can be used during the computation of the pairwise scoring function for limiting the search of interacting residues to those connected in the graph. In this way the computation of the pairwise potential only require a time that grows linearly with the protein size.